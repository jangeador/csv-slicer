__author__= "Delio Castillo"

from distutils.core import setup
import py2exe

packages=[]
data_files = [("settings", ["settings/settings.py", "settings/__init__.py"]),
              ("data", "")]
dll_excludes = ["w9xpopen.exe"]

setup(
    name='CSV Slicer',
    version='0.1',
    author='Delio Castillo',
    console=['csv-slicer'],
    options={
        "py2exe": {
            "bundle_files": 1,
            "dll_excludes": dll_excludes,
            "includes": packages,
        },
    },
    zipfile=None,
    data_files=data_files,
)
