__author__ = "Delio Castillo"

import yaml
from pandas.io.parsers import read_csv
import logging

config = []
output_folder = 'output'


def init():
    """
    Load the config file and then setup the logging

    """
    global config, output_folder
    with open('config.yaml') as f:
        config = yaml.load(f)
    output_folder = config['settings']['output_folder']
    logging.basicConfig(filename='%s/log.log' % output_folder, level=logging.DEBUG)


def process_file(input_file, index=None, separator=',', output_folder='output'):
    """
    This function processes the file specified on the config
    :param input_file:
    :param index:
    :param separator:
    :param output_folder:
    """
    df = read_csv(input_file,
                  sep=separator,
                  index_col=index,
                  header=0
                  )

    levels = [i for i in range(len(index))]

    if len(index) <= 1:
        for (state), group in df.groupby(level=0):
            group.to_csv('%s/%s.csv' % (output_folder, state))
    else:
        for index, group in df.groupby(level=levels):
            group.to_csv('%s/%s.csv' % (output_folder, "-".join(index)))


def main():
    """
    Load the config parameters and call the functions

    """
    init()
    for f in config["files"]:
        logging.info('Now processing %s' % f)
        process_file('%s/%s' % (f['path'], f['name']), index=f['index'])



if __name__ == '__main__':
    import sys
    sys.exit(main())