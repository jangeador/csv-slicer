from distutils.core import setup

setup(
    name='csv-slicer',
    version='0.1.0',
    author='Delio Castillo',
    author_email='jangeador@gmail.com',
    packages=['csv-slicer'],
    scripts=[],
    url='https://bitbucket.org/jangeador/csv-slicer/',
    license='LICENSE.txt',
    description='A csv slicer written in python.',
    long_description=open('README.md').read(),
    install_requires=['pandas', 'pyyaml']
)